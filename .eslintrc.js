module.exports = {
  env: {
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
  ],
  rules: {
    'comma-dangle': ['warn', 'always-multiline'],
    'semi': ['warn', 'always'],
  },
};
