module.exports = {
  content: [
    './index.html',
    './src/**/*.{js,vue}',
  ],
  theme: {
    extend: {
      colors: {
        blue: {
          600: '#253CF2',
        },
        gray: {
          100: '#F4F6F8',
          700: '#343A40',
        },
        indigo: {
          100: '#E5E8FD',
        },
      },
      transitionDuration: {
        250: '250ms',
      },
      transitionProperty: {
        button: 'background-color, fill, transform',
      },
      transitionTimingFunction: {
        'out-alt': 'cubic-bezier(0.125, 1.375, 0.125, 1)',
      },
    },
  },
  plugins: [],
};
