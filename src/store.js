import { createStore } from 'vuex';

const store = createStore({
  state() {
    return {
      upvoteGroups: [
        { id: 1, isSelected: false },
        { id: 2, isSelected: false },
        { id: 3, isSelected: false },
      ],
    };
  },
  getters: {
    getUpvoteGroup: state => {
      return id => state.upvoteGroups.find(group => group.id === id);
    },
  },
  mutations: {
    toggleUpvote(state, payload) {
      const groupIndex = state.upvoteGroups.findIndex(group => group.id === payload.id);
      const group = state.upvoteGroups[groupIndex];
      group.isSelected = !group.isSelected;
    },
  },
});

export default store;
